import boto3
import json
import os
from datetime import datetime, timedelta
from googleads import dfp, oauth2, errors
from boto3.dynamodb.conditions import Key, Attr

#AWS Setup 
dynamodb = boto3.resource('dynamodb')
s3 = boto3.client('s3')

# Google API Setup Info
appClientId = '728269833024-djfvj4rqlsgg4b29og60esl97hgc8kv5.apps.googleusercontent.com'
appClientSecret = 'xxxxxxx-xxxxxxxxxxx'
userRefreshToken = '' #We'll get this later from the user's stored connection
appName = 'Liteturn APP'


def getDFPClient(appClientId,appClientSecret,userRefreshToken,appName):
    try:
        oauth2_client = oauth2.GoogleRefreshTokenClient(appClientId, appClientSecret, userRefreshToken)
        dfp_client = dfp.DfpClient(oauth2_client, appName)
        return dfp_client
    except ValueError as e:
        raise ValueError()

def getConnection(account_id, connection_id):
    # This gets either all of the connections for an account or a specific connection if requested.
    connections = {}
    try:
        table = dynamodb.Table('Connections')
        response = table.query(
            ProjectionExpression="#act, #n, #s, #i, #con, #p, #rt, #nn, #nc",
            ExpressionAttributeNames={ "#act": "accountId", "#n": "name", "#s": "status", "#i":"image", "#con": "connectionId", "#p": "provider", "#rt": "refreshToken", "#nn":"networkName", "#nc":"networkCode"},
            KeyConditionExpression=Key("accountId").eq(account_id) & Key("connectionId").eq(connection_id)
        )
        connections = {"connections": response['Items']}
        return connections
    except ValueError as e:
        print(e.response['Error']['Message'])
        return {'response': 'Error getting connections'}



def lambda_handler(event, context):
    print('Loading function')
    message = json.loads(event['Records'][0]['Sns']['Message'])
    orderId = message['orderId']
    reportInstanceId = message['reportInstanceId']
    reportId = message['reportId']
    accountId = message['accountId']
    connectionId = message['connectionId']

    connection = getConnection(account_id=accountId,connection_id=connectionId)
    userRefreshToken = connection['connections'][0]['refreshToken']
    networkCode = connection['connections'][0]['networkCode']
    DFPClient = getDFPClient(appClientId,appClientSecret,userRefreshToken,appName)
    DFPClient.network_code = connection['connections'][0]['networkCode']


    values = [{
        'key': 'id',
        'value': {
            'xsi_type': 'NumberValue',
            'value': orderId
        }
    }]
    filter_statement = {'query': 'WHERE ORDER_ID = :id','values': values}
    end_date = datetime.now()
    start_date = end_date - timedelta(days=60)
    report_job = {
        'reportQuery': {
            'dimensions': ['ORDER_ID', 'ORDER_NAME', 'LINE_ITEM_ID', 'LINE_ITEM_NAME', 'LINE_ITEM_TYPE','DATE'],
            'dimensionAttributes': ['ORDER_TRAFFICKER', 'ORDER_START_DATE_TIME', 'ORDER_END_DATE_TIME', 'LINE_ITEM_START_DATE_TIME','LINE_ITEM_END_DATE_TIME','LINE_ITEM_COST_TYPE','LINE_ITEM_COST_PER_UNIT','LINE_ITEM_GOAL_QUANTITY'],
            'statement': filter_statement,
            'columns': ['AD_SERVER_IMPRESSIONS', 'AD_SERVER_CLICKS'],
            'dateRangeType': 'CUSTOM_DATE',
            'startDate': {'year': start_date.year,'month': start_date.month,'day': start_date.day},
            'endDate': {'year': end_date.year,'month': end_date.month,'day': end_date.day}
        }
    }
    # Initialize a DataDownloader.
    report_downloader = DFPClient.GetDataDownloader(version='v201702')
    try:
        # Run the report and wait for it to finish.
        report_job_id = report_downloader.WaitForReport(report_job)
    except errors.DfpReportError, e:
        print 'Failed to generate report. Error was: %s' % e
    export_format = 'CSV_DUMP'
    filename = '/tmp/'+reportInstanceId+".csv.gz"
    report_file = open(filename, "w+")
    #Download report data.
    report_downloader.DownloadReportToFile(report_job_id, export_format, report_file)
    report_file.close()#
    # Set the bucket info and upload the data file to s3
    s3location = accountId+"/"+reportId+"/"+reportInstanceId+".csv.gz"
    s3bucket = "liteturn-raw"
    try:
        s3.upload_file(Filename=filename, Bucket=s3bucket, Key=s3location)
        # Display results.
        print 'File uploaded to S3 at: %s/%s' % (s3bucket,s3location)
        os.remove(filename)
        return(True)
    except Exception as e:
        print(e)
        print('Error putting object {} to bucket {}.'.format(key, bucket))
        raise e







